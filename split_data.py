import json
import os
import shutil

import constant

def load_jsonl(path):
    shutil.rmtree(constant.STORAGE_FOLDER)
    with open(path, 'r', encoding='utf-8') as reader:
        for data in reader:
            export_keys(json.loads(data))
    

def export_keys(data):
    keys = []
    solutions = []
    problem = ''
    detail = ''

    labels = data['labels']
    content = data['text']

    if len(labels) == 0: return

    for label in labels:
        if label[2] == 'Key':
            keys.append(content[label[0] : label[1]])
            continue

        if label[2] == 'Solution':
            solutions.append(content[label[0] : label[1]])
            continue

        if label[0] == 0:
            problem = label[2]
            if problem == '': return
            continue
        
        else:
            detail = label[2]
            continue  


    path = constant.STORAGE_FOLDER + '/' + problem + '/' + detail

    if not os.path.isdir(path):
        os.makedirs(path)

        	
    with open(path + "/data.txt", 'a+', encoding='utf-8') as writter:
        print(path)
        for key in keys:
            writter.write(key + '\n')
    


if __name__ == "__main__":
    load_jsonl(constant.LABELED_JSON_PATH)
